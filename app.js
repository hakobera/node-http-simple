var http = require('http');

var server = http.createServer(function (req, res) {
  res.writeHead(200, { "Content-Type": "text/plain" });
  res.end(JSON.stringify(process.env));
})
.listen(3000, function () {
 console.log('server listen on port %d', server.address().port);
});
